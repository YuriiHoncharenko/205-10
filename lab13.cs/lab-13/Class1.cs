﻿using System;

public class Tablet
{
    public string Name { get; set; }
    public string Manufacturer { get; set; }
    public string Colour { get; set; }
    public int Memory { get; set; }
    public int Amount { get; set; }
    public double Cost { get; set; }
    public int Users { get; set; }
    public double GetYearIncomePerInhabitant()
    {
        return Cost * Users;
    }
    public Tablet()
    {
    }
    public Tablet(string name, string manufacturer, string colour, int memory, int amount, double cost, int users)
    {
        Name = name;
        Manufacturer = manufacturer;
        Colour = colour;
        Memory = memory;
        Amount = amount;
        Cost = cost;
        Users = users;
    }

}

