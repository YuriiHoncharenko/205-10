﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace lab13
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Припинити роботу застосунку?", "Припинити роботу", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit();
        }

        private void fMain_Load(object sender, EventArgs e)
        {
            gvTablets.AutoGenerateColumns = false;
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Name";
            column.Name = "Назва планшету";
            gvTablets.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Manufacturer";
            column.Name = "Виробник";
            gvTablets.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Colour";
            column.Name = "Колір";
            gvTablets.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Memory";
            column.Name = "Кількість пам'яті";
            gvTablets.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Amount";
            column.Name = "Кількість планшетів";
            gvTablets.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Cost";
            column.Name = "Ціна";
            gvTablets.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Users";
            column.Name = "Кількість користувачів";
            gvTablets.Columns.Add(column);
            EventArgs args = new EventArgs();
            OnResize(args);


        }
        private void btnTablet_Click(object sender, EventArgs e)
        {
            Tablet tablet = new Tablet();
            fTablet fb = new fTablet(tablet);
            if (fb.ShowDialog() == DialogResult.OK)
            {
                bindingSource1.Add(tablet);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Tablet tablet = (Tablet)bindingSource1.List[bindingSource1.Position];
            fTablet fb = new fTablet(tablet);
            if (fb.ShowDialog() == DialogResult.OK)
            {
                bindingSource1.List[bindingSource1.Position] = tablet;
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Видалити поточний запис?", "Видалення запису", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                bindingSource1.RemoveCurrent();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Очистити таблицю?\n\nВсі дані будуть втрачені", "Очищення даних", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                bindingSource1.Clear();
            }
        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрити застосунок?", "Вихід з програми", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.Exit();
            }


        }
        private void fMain_Resize(object sender, EventArgs e)
        {
            int buttonsSize = 9 * btnTablet.Width + 3 * toolStripSeparator1.Width;
            btnExit.Margin = new Padding(Width - buttonsSize, 0, 0, 0); 
        }

        private void btnSaveAsText_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Текстові файли (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.Title = "Зберегти дані у текстовомуформаті"; 
            saveFileDialog.InitialDirectory =Application.StartupPath;
            StreamWriter sw;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                sw = new StreamWriter(saveFileDialog.FileName, false, Encoding.UTF8);
                try
                {
                    foreach (Tablet tablet in bindingSource1.List)
                    {
                        sw.Write(tablet.Name + "\t" + tablet.Manufacturer + "\t" + tablet.Colour + "\t" + tablet.Memory + "\t" + tablet.Amount + "\t" + tablet.Cost + "\t" + tablet.Users + "\t\n");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    sw.Close();
                }
            }
        }

        private void btnSaveAsBinary_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Файли даних (*.tablets)|*.tablets|All files (*.*)|*.*";
            saveFileDialog.Title = "Зберегти дані у бінарному форматі";
            saveFileDialog.InitialDirectory = Application.StartupPath;
            BinaryWriter bw;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                bw = new
               BinaryWriter(saveFileDialog.OpenFile()); try
                {
                    foreach (Tablet tablet in bindingSource1.List)
                    {
                        bw.Write(tablet.Name);
                        bw.Write(tablet.Manufacturer);
                        bw.Write(tablet.Colour);
                        bw.Write(tablet.Memory);
                        bw.Write(tablet.Amount);
                        bw.Write(tablet.Cost);
                        bw.Write(tablet.Users);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    bw.Close();
                }
            }
        }

        private void btnOpenFromText_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Текстові файли (*.txt)|*.txt|All files(*.*)|*.*";
            openFileDialog.Title = "Прочитати дані у текстовому форматі";
            openFileDialog.InitialDirectory = Application.StartupPath;
            StreamReader sr;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bindingSource1.Clear();
                sr = new StreamReader(openFileDialog.FileName, Encoding.UTF8);
                string s;
                try
                {
                    while ((s = sr.ReadLine()) != null)
                    {
                        string[] split = s.Split('\t');
                        Tablet tablet = new Tablet(split[0], split[1], split[2], int.Parse(split[3]), int.Parse(split[4]), double.Parse(split[5]), int.Parse(split[6])); 
                        bindingSource1.Add(tablet);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    sr.Close();
                }
            }
        }

        private void btnOpenFromBinary_Click(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Файли даних (*.tablets)|*.tablets|All files (*.*)|*.*";
            openFileDialog.Title = "Прочитати дані у бінарному форматі";
            openFileDialog.InitialDirectory = Application.StartupPath;
            BinaryReader br;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bindingSource1.Clear();
                br = new BinaryReader(openFileDialog.OpenFile());
                try
                {
                    Tablet tablet;
                    while (br.BaseStream.Position < br.BaseStream.Length)
                    {
                        tablet = new Tablet();
                        for (int i = 1; i <= 8; i++)
                        {
                            switch (i)
                            {
                                case 1:
                                    tablet.Name = br.ReadString();
                                    break;
                                case 2:
                                    tablet.Manufacturer = br.ReadString();
                                    break;
                                case 3:
                                   tablet.Colour = br.ReadString();
                                    break;
                                case 4:
                                    tablet.Memory = br.ReadInt32();
                                    break;
                                case 5:
                                    tablet.Amount = br.ReadInt32();
                                    break;
                                case 6:
                                   tablet.Cost = br.ReadDouble();
                                    break;
                                case 7:
                                    tablet.Users = br.ReadInt32();
                                    break;
                            }
                        }
                        bindingSource1.Add(tablet);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message,
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    br.Close();
                }
            }
        }

        private void gvTablets_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
