﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace lab08
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }


        private void btnExit_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Припинити роботу застосунку?","Припинити роботу", MessageBoxButtons.OKCancel,MessageBoxIcon.Question) == DialogResult.OK)
                Application.Exit(); 
        }

        private void fMain_Load(object sender, EventArgs e)
        {
            gvTablets.AutoGenerateColumns = false;
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Name";
            column.Name = "Назва планшету";
            gvTablets.Columns.Add(column);  
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Manufacturer";
            column.Name = "Виробник";
            gvTablets.Columns.Add(column); 
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Colour";
            column.Name = "Колір";
            gvTablets.Columns.Add(column); 
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Memory";
            column.Name = "Кількість пам'яті";
            gvTablets.Columns.Add(column); 
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Amount";
            column.Name = "Кількість планшетів";
            gvTablets.Columns.Add(column); 
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Cost";
            column.Name = "Ціна";
            gvTablets.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Users";
            column.Name = "Кількість користувачів";
            gvTablets.Columns.Add(column);
            EventArgs args = new EventArgs();
            OnResize(args);
        }
        private void btnBook_Click(object sender, EventArgs e)
        {
            Tablet tablet = new Tablet();
            fTablet fb = new fTablet(tablet);
            if (fb.ShowDialog() == DialogResult.OK)
            {
                bindingSource1.Add(tablet);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Tablet tablet = (Tablet)bindingSource1.List[bindingSource1.Position]; 
            fTablet fb = new fTablet(tablet);
            if (fb.ShowDialog() == DialogResult.OK)
            {
                bindingSource1.List[bindingSource1.Position] = tablet;
            } 
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Видалити поточний запис?", "Видалення запису", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
            {
                bindingSource1.RemoveCurrent();
            } 
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Очистити таблицю?\n\nВсі дані будуть втрачені","Очищення даних", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                bindingSource1.Clear();
            } 
        }

        private void btnExit_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрити застосунок?", "Вихід з програми", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.Exit(); 
            }


        }

    }
}
