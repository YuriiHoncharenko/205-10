﻿using System;

public class Tablet
{
    public string Name;
    public string Manufacturer;
    public string Colour;
    public int Memory;
    public int Amount;
    public int Users;
    public double Cost;
    public double YearInCome;
    public double GetYearIncomePerInhabitant()
    {
        return Cost * Users;
    }
}
